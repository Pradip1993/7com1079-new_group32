---
title: "Price of Houses in Melbourne"
author: "Team Project"
date: "10/06/2021"
output:
  word_document: default
  html_document: default
---

# Melbourne Housing Market
## Melbourne housing clearance data from Jan 2016

### Content & Acknowledgements
This data was scraped from publicly available results posted every week from Domain.com.au, I've cleaned it as best I can, now it's up to you to make data analysis magic. The dataset includes Address, Type of Real estate, Suburb, Method of Selling, Rooms, Price, Real Estate Agent, Date of Sale and distance from C.B.D.

## The data was scraped from publicly available results posted every week from Domain.com.au. The variables used in our analysis are as follows:
### Some Key Details
* Suburb: Suburb
* Address: Address
* Rooms: Number of rooms
* Price: Price in Australian dollars

### Method:
* S - property sold;
* SP - property sold prior;
* PI - property passed in;
* PN - sold prior not disclosed;
* SN - sold not disclosed;
* NB - no bid;
* VB - vendor bid;
* W - withdrawn prior to auction;
* SA - sold after auction;
* SS - sold after auction price not disclosed.
* N/A - price or highest bid not available.

### Type:
* br - bedroom(s);
* h - house,cottage,villa, semi,terrace;
* u - unit, duplex;
* t - townhouse;
* dev site - development site;
* o res - other residential.

### Attributes:
* **SellerG:** Real Estate Agent
* **Date:** Date sold
* **Distance:** Distance from CBD in Kilometres
* **Regionname:** General Region (West, North West, North, North east …etc)
* **Propertycount:** Number of properties that exist in the suburb.
* **Bedroom2:** Scraped # of Bedrooms (from different source)
* **Bathroom:** Number of Bathrooms
* **Car:** Number of carspots
* **Landsize:** Land Size in Metres
* **BuildingArea:** Building Size in Metres
* **YearBuilt:** Year the house was built
* **CouncilArea:** Governing council for the area
* **Lattitude:** Self explanitory
* **Longtitude:** Self explanitory

# Importing Melbourne Housing Market dataset from Kaggle
Real estate is “property consisting of land and the buildings on it, along with it natural resources such as crops, minerals or water”. The business of real estate is the profession of buying, selling, or renting land, buildings or housing. Residential real estate may contain either a single family or multifamily structure that is available for occupation or for non-business purposes. Residences can be classified by if and how they are connected to neighbouring residences and land -
```{r}
# Link: https://www.kaggle.com/anthonypino/melbourne-housing-market
data <- read.csv("E:/MLDataSets/Melbourne_housing_FULL.csv")
```
# Inspect Melbourne Housing Market dataset
```{r}
typeof(data)
mode(data)
class(data)
nrow(data)
ncol(data)
dim(data)
str(data)
head(data)
tail(data)
# My field study concerns the housing rates in the various regions of Melbourne, Australia. As of November 2017, Melbourne’s house price growth has exceeded Sydney’s for the previous twelve months with signs of renewed momentum in the latter half of that period after showing signs of slowdown. Experts are predicting the slowed trends to continue in 2018 due to APRA’s (Australian Prudential Regulatory Authority) tightening on investor borrowing and interest-only loans which has also led to less investors. This also indicates a possibility of a comeback of first-time property buyers who can now get low-interest loans more easily than before. On these lines, I believe that it would be extremely interesting to study what factors influence the housing prices in certain highly populated regions.
```
# An empirical study of the house prices in various regions of Melbourne
Melbourne is currently experiencing a housing bubble (some experts say it may burst soon). The specific objective of this study is to identify the influence of “Distance from CBD (Central Business District)” and “Landsize” that affect the prices of houses in Melbourne. The dataset includes Address, Type of Real estate, Suburb, Method of Selling, Rooms, Price, Real Estate Agent, Date of Sale and distance from C.B.D.

Hypothesis H1 - The price of houses is correlated with Distance from CBD and landsize.

# Analysing the Melbourne Housing Market data
```{r}
summary(data)

table(data$Type)
prop.table(table(data$Type))
prop.table(table(data$Type))*100

table(data$Rooms)
prop.table(table(data$Rooms))
prop.table(table(data$Rooms))*100

table(data$Car)
prop.table(table(data$Car))
prop.table(table(data$Car))*100

table(data$Type, data$Rooms)
prop.table(table(data$Type, data$Rooms))
prop.table(table(data$Type, data$Rooms))*100
```
# Data Cleansing
Here we see that there are a lot of missing and junk values in the data set which we need to cleanse. For Eg., Price, Bedroom2 and Bathroom have quite a lot of “NA” values (missing) and Landsize has a lot junk values such as 0,1,3,10 and so on, which are obviously too less to build a house on.

So, to cleanse our dataset, we do the following: 1. Filter out the NA values in the columns - Price, Bedroom2, Bathroom, Car and YearBuilt 2. Filter out all data that has landsize < 121. The reason behind this figure of 121 : Considering that landsize has been mentioned as square units, we take 121 i.e. square of 11 as a reasonable entity to build our house on.
```{r}
housing <- data[which(data$Price != 'NA' & data$Bedroom2 != 'NA' & data$Bathroom != 'NA' & data$Car != 'NA' & data$YearBuilt != 'NA' & data$Landsize > 120),]
housing$Distance <- as.numeric(as.character(housing$Distance)) ## to change datatype of Distance from Factor to Numeric
lmodel <- lm(Price ~ Distance + Bedroom2 + Bathroom + Car + Landsize + YearBuilt, data = housing)
summary(lmodel)
# We established the effect of various factors on the price of houses in Melbourne with the simplest model. We regressed Price on no.of rooms, car slots, YearBuilt and distance of CBD. We estimated model, using linear least squares.
```
We found empirical support for H1. The F-statistic and overall p-value (2.2e-16 < 0.05) suggests that the model is overall a good model. The value of adjusted R-squared (=0.45) suggests that the selected explanatory variables explain 45% of the variance while also suggesting that there are few other variables that account for the price. Further, the beta-coefficients of all explanatory variables have a significant p-value (<0.05) which suggests all of them are correlated with Price.

This paper was motivated by the need for research that could improve our understanding of how various factors influences the prices of houses in the Melbourne real-estate industry. We found that Distance from CBD is a inversely proportional to prices while all other factors are also correlated to the price.

# Descriptive Statistics analysis after cleaning the data
```{r}
typeof(housing)
mode(housing)
class(housing)
nrow(housing)
ncol(housing)
dim(housing)
head(housing)
tail(housing)
summary(housing)
# Now we see that there are no more ‘NA’ values and even the Landsizes look more practical for consideration. We have 21 columns and 8778 rows of data.
```

# Summary Statistics of the dataset
To check for the datatypes in columns and also to get the summary statistics for numerical variables:
```{r}
housing$Distance <- as.numeric(as.character(housing$Distance)) ## to change datatype of Distance from Factor to Numeric
str(housing)
```

# Contingency tables
To check for number of different types of property per region:
```{r}
region_by_type <- xtabs(~ Regionname+Type, data = housing)
region_by_type

```
Here, Type is the type of house in the dataset: h -> house,cottage,villa, semi,terrace; t -> townhouse; u -> unit, duplex;

To check for status of property sale per type per region:

```{r}
sell_type_region <- xtabs(~ Regionname+Type+Method, data = housing)
ftable(sell_type_region)

```
# Boxplot to check the data and outliers
To draw a boxplot to check for price in different regions:
```{r}
par(mar=c(3.1,12,4.1,2.1), mgp = c(11, 1, 0))
boxplot(housing$Price ~ housing$Regionname, horizontal = TRUE, ylab = "Region Name", xlab = "Price of houses", main = "Boxplot of price of houses by region", las = 1)
# Insights:Western Victoria has lowest median house price whereas Southern Metropolitan has highest median house price.
# There is a shockingly high outlier in South-Eastern Metropolitan region
```
To draw a boxplot to check for landsize in different regions:

```{r}
par(mar=c(3.1,12,0.95,2.1), mgp = c(11, 1, 0), mfrow = c(2,1))
boxplot(housing$Landsize ~ housing$Regionname, horizontal = TRUE, xlab = "Landsize of houses", ylab = "Region Name", main = "Boxplot of landsize of houses by region", las = 1)
boxplot(housing$Distance ~ housing$Type, horizontal = TRUE, ylab = "Type of House", xlab = "Distance from CBD", main = "Boxplot of distance from CBD vs type of houses", las = 1)

```

# Histograms
```{r}
par(mfrow = c(2,2), mar=c(3.1,3.1,0.95,0))
hist(housing$Distance, breaks = 40, xlim = c(0,50), ylim = c(0,800),xlab = "Distance from CBD", col = "Green", main = "Histogram of Distances from CBD", las =1)
hist(housing$Bedroom2, breaks = 40, xlim = c(0,10), ylim = c(0,4000),xlab = "No. of Bedrooms", col = "Red", main = "Histogram of no. of bedrooms in houses", las =1)
hist(housing$Bathroom, breaks = 40, xlim = c(0,10), ylim = c(0,4000),xlab = "No. of Bathrooms", col = "Blue", main = "Histogram of no. of bathrooms in houses", las =1)
hist(housing$Car, breaks = 40, xlim = c(0,10), ylim = c(0,4000),xlab = "No. of Carslots", col = "gray33", main = "Histogram of no. of carslots in houses", las =1)
# From the Distance from CBD histogram, we can see a very strong right skew in the data indicating that very few of the houses are extremely far away from CBD. From the remaining 3 histograms, we can infer that the majority of houses have 2-4 bedrooms, 1-2 bathrooms and 1-2 carslots. However, there are outliers in each histogram
```

# Correlation Matrix
To get the correlation matrix for all numeric variables in the dataset, rounded to 2 decimal places:
```{r}
round(cor(housing[,c(3,5,9,11,12,13,14,15,16,18,19)]),2)
```
# Hypothesis Testing
# Testing Correlation between Distance from CBD and Price of houses
Null Hypothesis: There is no significant difference between Distance from CBD and Price of houses i.e. there is no correlation between the two variable. Alternative Hypothesis: There is a significant difference between Distance from CBD and price and they are correlated.
```{r}
# Using the Pearson’s Correlation test:
cor.test(housing$Price, housing$Distance)
```
We have a significant p-value here (2.2e-16 << 0.05) and thus we reject the null hypothesis. We conclude there is indeed a correlation between distance from CBD and price.

## Testing Correlation between landsize and Price of houses
Null Hypothesis: There is no significant difference between landsize and Price of houses i.e. there is no correlation between the two variable. Alternative Hypothesis: There is a significant difference between landsize and price and they are correlated.
```{r}
# Using the Pearson’s Correlation test:
cor.test(housing$Price, housing$Landsize)
```
p-value = 0.6881(>0.05) is not significant and suggests that we fail to reject the null hypothesis that there is no correlation between land size and price.

# Testing Correlation between no. of rooms and Price of houses
Null Hypothesis: There is no significant difference between No. of rooms and Price of houses i.e. there is no correlation between the two variable. Alternative Hypothesis: There is a significant difference between No. of rooms and price and they are correlated.
```{r}
# Using the Pearson’s Correlation test:
cor.test(housing$Price, housing$Rooms)
```
Here again we have a significant p-value here (2.2e-16 << 0.05) and thus we reject the null hypothesis. We conclude there is indeed a correlation between No. of rooms and price.